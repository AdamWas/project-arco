using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemBase : MonoBehaviour
{
    public string type;
    public string nameOfItem;
    public int count = 1;

    public abstract void OnPickUp();
    public abstract void OnAttackBehaviour();
    public abstract void OnDamagedBehaviour();
    public abstract void OnKillBehaviour();
    public abstract void OnDodgeBehaviour();
    public abstract void OnDeathBehaviour();

}
