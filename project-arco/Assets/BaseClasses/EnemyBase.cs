using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBase : MonoBehaviour
{
    public PlayerClass _player;
    public GameObject _enemy;
    public Slider _healthBar;
    public GameObject[] _objects;

    public float maxHealth = 50.0f;
    public float currentHealth = 50.0f;
    public float damageValue = 10.0f;

    public void Start()
    {
        _objects[0].SetActive(false);
        _objects[1].SetActive(false);
        _objects[2].SetActive(false);
    }

    public void Update()
    {
        RotateHealthBarToPlayer();
    }

    public void Attack()
    {

    }

    public void TakeDamage(float damageTaken)
    {
        this.currentHealth -= damageTaken;
        ChangeHealthBarValue();
        if (currentHealth < 0)
            OnDeath();
    }

    public void OnDeath()
    {
        _player.ItemOnKill();
        Destroy(this.gameObject);
        _objects[0].SetActive(true);
    }

    public void ChangeHealthBarValue()
    {
        _healthBar.value = (currentHealth / maxHealth) * 100;
    }

    public void RotateHealthBarToPlayer()
    {
        Vector3 rotationVector = _player.transform.position - _healthBar.transform.position;
        rotationVector.Normalize();
        _healthBar.transform.rotation = Quaternion.Slerp(_healthBar.transform.rotation,
            Quaternion.LookRotation(rotationVector), 15 * Time.deltaTime);
    }

    public void Death()
    {
        Destroy(_enemy);
    }

    public void SeekPlayer()
    {

    }
}



