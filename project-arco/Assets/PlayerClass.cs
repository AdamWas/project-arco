using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass : MonoBehaviour
{

    //Movment Atr
    public float X_axisSpeed = 30.0f;
    public float Z_axisSpeed = 30.0f;
    public float Y_rotationSpeed = 240.0f;
    public float jumpHeight = 50.0f;

    //Combat Atr
    public float playerMaxHealth = 100.0f;
    public float playerCurrentHealth = 100.0f;
    public float playerDamageValue = 30.0f;

    //Misc
    public Rigidbody _rigidbody;
    public BoxCollider _playerHurtBox;
    public GameObject _enemyGameObject = null;
    bool isInAir = false;

    //Items
    public List<ItemBase> itemList;

    //Abilities Atr
    GameObject pocisk = null;
    GameObject sfera = null;
    float distance = 5000.0f;

    void Start()
    {
        //Inicjalizacjia listy przedmiot�w
        itemList = new List<ItemBase>();
        Cursor.lockState = CursorLockMode.Locked;  
    }

    void Update()
    {
        Movement();
        Attack();
        abilities();
        if (Input.GetKey(KeyCode.Q))
        {
            Application.Quit();
        }
    }

    public void CheckIfCurrentHealthIsGreaterThanMax()
    {
        if (this.playerCurrentHealth > this.playerMaxHealth)
            this.playerCurrentHealth = this.playerMaxHealth;
    }
    public void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            _playerHurtBox.enabled = true;
            if (_enemyGameObject != null)
            {
                //ITEM CHECK
                ItemOnAttack();


                if (this._enemyGameObject.GetComponent<EnemyBase>() != null)
                    this._enemyGameObject.GetComponent<EnemyBase>().TakeDamage(this.playerDamageValue);
            }
            _enemyGameObject = null;
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            _playerHurtBox.enabled = false;
        }

    }

    public void TakeDamage(float damageTaken)
    {
        this.playerCurrentHealth -= damageTaken;
        ItemOnDamaged();
        CheckIfPlayerDead();
    }
    public void CheckIfPlayerDead()
    {
        if (this.playerCurrentHealth <= 0)
        {
            ItemOnDeath();

            if (this.playerCurrentHealth <= 0)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public void Movement()
    {
        //Horizontal Movement
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            //Dashes
            if (Input.GetKey(KeyCode.W))
            {
                Debug.Log("Dash_F");
                ItemOnDodge();
                this.transform.Translate(new Vector3(X_axisSpeed * 50, 0, 0) * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                Debug.Log("Dash_B");
                ItemOnDodge();
                this.transform.Translate(new Vector3(-X_axisSpeed * 50, 0, 0) * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                Debug.Log("Dash_L");
                ItemOnDodge();
                this.transform.Translate(new Vector3(0, 0, Z_axisSpeed * 50) * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                Debug.Log("Dash_R");
                ItemOnDodge();
                this.transform.Translate(new Vector3(0, 0, -Z_axisSpeed * 50) * Time.deltaTime);
            }
        }
        else
        {
            //Normal Movement
            if (Input.GetKey(KeyCode.W))
            {
                this.transform.Translate(new Vector3(X_axisSpeed, 0, 0) * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                this.transform.Translate(new Vector3(-X_axisSpeed, 0, 0) * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                this.transform.Translate(new Vector3(0, 0, Z_axisSpeed) * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.transform.Translate(new Vector3(0, 0, -Z_axisSpeed) * Time.deltaTime);
            }
        }

        //Jump
        if (Input.GetKey(KeyCode.Space))
        {
            if (!isInAir)
            {
                isInAir = true;
                this._rigidbody.AddForce(new Vector3(0, jumpHeight, 0));
            }
            else
            {
                Debug.Log(isInAir);
                if (this._rigidbody.velocity.y == 0)
                {
                    isInAir = false;
                }
            }

        }

        //Rotation
        if (Input.GetAxis("Mouse X") > 0)
        {
            transform.Rotate(new Vector3(0, Y_rotationSpeed, 0) * Time.deltaTime);
        }
        else if (Input.GetAxis("Mouse X") < 0)
        {
            transform.Rotate(new Vector3(0, -Y_rotationSpeed, 0) * Time.deltaTime);
        }
    }

    //Abilities

    public void ability_1()
    {
        if (pocisk == null && Input.GetKeyDown(KeyCode.Alpha1))
        {
            pocisk = new GameObject("pocisk");
            Vector3 newObjectLocation = (this.transform.right * 1.0f) + this.transform.position;
            newObjectLocation[1] *= 0.5f;
            pocisk.transform.Translate(newObjectLocation);
            pocisk.transform.Rotate(90, transform.eulerAngles[1], 90);

            BoxCollider bc = pocisk.AddComponent<BoxCollider>();
            MeshFilter mf = pocisk.AddComponent<MeshFilter>();
            MeshRenderer mr = pocisk.AddComponent<MeshRenderer>();

            var temp_go = Resources.Load<GameObject>("projectile");

            mf.mesh = temp_go.GetComponent<MeshFilter>().sharedMesh;
            bc.size = new Vector3(0.1f, 0.8f, 0.1f);
            mr.material = temp_go.GetComponent<MeshRenderer>().sharedMaterial;
        }
        else if (pocisk != null)
        {
            if (distance >= 0)
            {
                pocisk.transform.Translate(new Vector3(0, -5.0f, 0) * Time.deltaTime);
                distance -= 5.0f;
            }
            else
            {
                Destroy(pocisk);
                pocisk = null;
                distance = 5000.0f;
            }
        }
    }

    public void ability_2()
    {
        if (sfera == null && Input.GetKeyDown(KeyCode.Alpha2))
        {
            sfera = new GameObject("sfera");
            sfera.transform.Translate(this.transform.position);

            MeshFilter mf = sfera.AddComponent<MeshFilter>();
            MeshRenderer mr = sfera.AddComponent<MeshRenderer>();

            var temp_go = Resources.Load<GameObject>("sfera");

            mf.mesh = temp_go.GetComponent<MeshFilter>().sharedMesh;
            mr.material = temp_go.GetComponent<MeshRenderer>().sharedMaterial;
        }
        else if (sfera != null)
        {
            Vector3 scaleChange = new Vector3(2.0f, 2.0f, 2.0f);
            sfera.transform.localScale += scaleChange;
            if (sfera.transform.localScale.x >= 500)
            {
                Destroy(sfera);
            }
        }
    }

    public void ability_3()
    {
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            playerCurrentHealth += 30.0f;
            if (playerCurrentHealth > 100.0f)
            {
                playerCurrentHealth = 100.0f;
            }
        }
    }

    public void ability_4()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            X_axisSpeed = 30.0f;
            Z_axisSpeed = 30.0f;
        }
        if (X_axisSpeed > 10.0f)
        {
            X_axisSpeed -= 0.01f;
            Z_axisSpeed -= 0.01f;
        }
        if (X_axisSpeed < 10.0f)
        {
            X_axisSpeed = 10.0f;
            Z_axisSpeed = 10.0f;
        }
    }

    public void abilities()
    {
        ability_1();
        ability_2();
        ability_3();
        ability_4();
    }




    //ItemsBehaviour
    public void AddItem(ItemBase item)
    {
        this.itemList.Add(item);
        item.OnPickUp();
    }
    public void ItemOnAttack()
    {
        for (int i = 0; i < itemList.Count; i++)
            itemList[i].OnAttackBehaviour();
    }
    public void ItemOnDamaged()
    {
        for (int i = 0; i < itemList.Count; i++)
            itemList[i].OnDamagedBehaviour();
    }
    public void ItemOnKill()
    {
        for (int i = 0; i < itemList.Count; i++)
            itemList[i].OnKillBehaviour();
    }
    public void ItemOnDodge()
    {
        for (int i = 0; i < itemList.Count; i++)
            itemList[i].OnDodgeBehaviour();
    }
    public void ItemOnDeath()
    {
        for (int i = 0; i < itemList.Count; i++)
            itemList[i].OnDeathBehaviour();
    }

}
