using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    public Vector3 PlayerStartPosition;
    void Start()
    {
        PlayerStartPosition = GameObject.Find("Player").transform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            other.transform.position = PlayerStartPosition;
        }
    }
}
