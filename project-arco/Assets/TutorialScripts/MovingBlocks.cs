using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlocks : MonoBehaviour
{
    Vector3 StartPos;
    public float range = 15f;
    public float speed = 15f;

    void Start()
    {
        StartPos = this.transform.position;
    }

    void Update()
    {
        MoveBlocks();
    }

    void MoveBlocks()
    {
        this.transform.Translate(new Vector3(-speed, 0, 0) * Time.deltaTime);

        if (this.transform.position.x < StartPos.x - range)
            speed *= -1;

        else if (this.transform.position.x > StartPos.x + range)
            speed *= -1;
    }
}
