using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinScript : MonoBehaviour
{
    Vector3 startPos;
    public float maxHeight;
    public float minHeight;
    public float flightSpeed;
    public float rotationSpeed;
    public bool ascending;


    void Start()
    {
        SetStartValues();
    }

    void Update()
    {
        UpAndDown();
        Rotation();
    }

    public void SetStartValues()
    {
        startPos = this.transform.position;
        maxHeight = 0.1f;
        minHeight = -0.1f;
        rotationSpeed = 0.2f;
        flightSpeed = 0.3f;
        ascending = true;
    }
    public void UpAndDown()
    {
        if (ascending)
        {
            if (this.transform.position.y < startPos.y + maxHeight)
            {
                Vector3 newPosition = this.transform.position;
                newPosition.y += flightSpeed * Time.deltaTime;
                this.transform.position = newPosition;
            }
            else
                ascending = false;
        }
        else if (!ascending)
        {
            if (this.transform.position.y > startPos.y + minHeight)
            {
                Vector3 newPosition = this.transform.position;
                newPosition.y -= flightSpeed * Time.deltaTime;
                this.transform.position = newPosition;
            }
            else
                ascending = true;
        }
    }
    public void Rotation()
    {
        this.transform.Rotate(new Vector3(0, rotationSpeed, 0));
    }
}
