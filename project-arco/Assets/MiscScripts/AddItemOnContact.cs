using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItemOnContact : MonoBehaviour
{
    public ItemBase item;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            var player = other.gameObject.GetComponent<PlayerClass>();
            player.AddItem(item);
            Destroy(this.gameObject);
        }
    }
}
