using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour
{
    public void goBack()
    {
        SceneManager.LoadScene(0);
        Debug.Log("OptionsMenu -> goBack()");
    }

}
