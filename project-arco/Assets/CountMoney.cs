using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountMoney : MonoBehaviour
{
    static public int money;
    public Text TextMoney;
    void Start()
    {
        money = 0;
    }

    public ItemBase item;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            money ++;
            var player = other.gameObject.GetComponent<PlayerClass>();
            player.AddItem(item);
            Destroy(this.gameObject);
            TextMoney.text = "" + money;
        }
    }
}
