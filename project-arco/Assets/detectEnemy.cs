using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectEnemy : MonoBehaviour
{
    public PlayerClass _player;

    private void OnTriggerEnter(Collider other)
    {
        getEnemyHit(other.gameObject);
    }

    public void getEnemyHit(GameObject enemyHit)
    {
        _player._enemyGameObject = enemyHit;
    }
}
