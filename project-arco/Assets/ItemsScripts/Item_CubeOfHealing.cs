using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_CubeOfHealing : ItemBase
{
    public PlayerClass _player;
    public void Start()
    {
        type = "onAttack";
        nameOfItem = "Cube of Healing";
    }

    public override void OnPickUp()
    {
        
    }

    public override void OnAttackBehaviour()
    {
        Debug.Log("Cube of Healing");
        _player.playerCurrentHealth += 2;
        
    }
    public override void OnDamagedBehaviour()
    {

    }
    public override void OnKillBehaviour()
    {

    }

    public override void OnDodgeBehaviour()
    {

    }
    public override void OnDeathBehaviour()
    {

    }

}
