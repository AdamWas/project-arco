using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillOfStrength : ItemBase
{
    public PlayerClass _player;

    public void Start()
    {
        type = "onKill";
        nameOfItem = "Pill of Strength";
    }

    public override void OnPickUp()
    {
        _player.playerDamageValue += 5;
    }
    public override void OnAttackBehaviour()
    {
        

    }
    public override void OnDamagedBehaviour()
    {

    }
    public override void OnKillBehaviour()
    {
        Debug.Log("Pill of Strength");
        _player.playerDamageValue += 3;
    }

    public override void OnDodgeBehaviour()
    {

    }
    public override void OnDeathBehaviour()
    {

    }
}
