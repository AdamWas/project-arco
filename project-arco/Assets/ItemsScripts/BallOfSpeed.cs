using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallOfSpeed : ItemBase
{
    public PlayerClass _player;

    public void Start()
    {
        type = "onKill";
        nameOfItem = "Ball of Speed";
    }

    public override void OnPickUp()
    {
        _player.X_axisSpeed += 5;
    }
    public override void OnAttackBehaviour()
    {


    }
    public override void OnDamagedBehaviour()
    {

    }
    public override void OnKillBehaviour()
    {
        Debug.Log(nameOfItem);
        _player.X_axisSpeed += 3;
    }

    public override void OnDodgeBehaviour()
    {

    }
    public override void OnDeathBehaviour()
    {

    }
}
